function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-inicio-inicio-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/inicio/inicio.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/inicio/inicio.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesInicioInicioPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n\n<ion-content>\n\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content pullingIcon=\"arrow-down\" pullingText=\"Tire para Actualizar\" refreshingSpinner=\"circles\">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <div *ngIf=\"home\">\n    <ion-card style=\"padding: 0px;margin-top:12%;\" *ngFor=\"let item of home;let i = index\">\n      <div (click)=\"verHome(item)\" *ngIf=\"!item.title.rendered == ''\">\n        <img [src]=\"item.featured_image.size_large\">\n        <h3 style=\"text-align: center;\" [innerHTML]=\"item.title.rendered\"></h3>\n        <h4 style=\"text-align: center;\">Precio: ${{precio[i].precio | number}}</h4>\n        <div style=\"text-align:justify;font-size:15px;padding:5px;\" [innerHTML]=\"item.excerpt.rendered | slice:0:150\">\n        </div>\n      </div>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <div></div>\n          </ion-col>\n          <ion-col>\n            <div>\n              <ion-button *ngIf=\"!verificarCantidad(i)\" \n                (click)=\"agregarCarrito(item.id,precio[i].precio,item.title.rendered,item.featured_image.size_large,i)\"\n                style=\"float: right\" size=\"small\">Agregar\n                <ion-icon name=\"add\"></ion-icon>\n              </ion-button>\n              <ion-col style=\"background: #6c6e70;float: right;border-radius: 5px;padding:0px\"\n                *ngIf=\"verificarCantidad(i)\">\n                <ion-grid style=\"padding: 0px\">\n                  <ion-row>\n                    <ion-col>\n                      <div>\n                        <ion-icon (click)=\"restar(i,item.cantidad,item.id)\" style=\"font-size: 30px;color: white;margin-left: 6%;\"\n                          name=\"remove-circle\"></ion-icon>\n                      </div>\n                    </ion-col>\n                    <ion-col>\n                      <div style=\"margin-top: 6%;\">\n                        <ion-label style=\"font-size: 20px;color: white;margin-left: 28%;width: 100%;\">\n                          {{item.cantidad >=0 ? item.cantidad : 0}}</ion-label>\n                      </div>\n                    </ion-col>\n                    <ion-col>\n                      <div>\n                        <ion-icon (click)=\"sumar(item.id,i,item.cantidad)\"\n                          style=\"font-size: 30px;color: white;float: right\" name=\"add-circle\"></ion-icon>\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-col>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </div>\n\n  <div *ngIf=\"home.length == 0\">\n    <ion-list>\n      <ion-item *ngFor=\"let number of [0,1,2,3,4,5,6,7]\" detail>\n        <ion-avatar slot=\"start\">\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </ion-avatar>\n        <ion-label>\n          <h2>\n            <ion-skeleton-text animated></ion-skeleton-text>\n          </h2>\n          <p>\n            <ion-skeleton-text animated style=\"width: 88%\"></ion-skeleton-text>\n          </p>\n          <p>\n            <ion-skeleton-text animated style=\"width: 44%\"></ion-skeleton-text>\n          </p>\n          <p>\n            <ion-skeleton-text animated style=\"width: 22%\"></ion-skeleton-text>\n          </p>\n        </ion-label>\n        <ion-note slot=\"end\"></ion-note>\n      </ion-item>\n    </ion-list>\n  </div>\n\n  <ion-fab *ngIf=\"home.length > 0\" vertical=\"top\" horizontal=\"start\" slot=\"fixed\" (click)=\"presentModal()\">\n    <ion-fab-button class=\"start-btn\" color=\"danger\">\n      <ion-icon class=\"iconSize\" style=\"color: white\" name=\"color-fill\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n  <ion-fab *ngIf=\"home.length > 0\" vertical=\"top\" horizontal=\"end\" slot=\"fixed\" (click)=\"pedido()\">\n    <ion-fab-button style=\"position: relative;\"  class=\"start-btn\" color=\"medium\">\n      <ion-icon style=\"font-size: 40px;color: white;margin-left: -5%;position: absolute;\" name=\"cart\"></ion-icon>\n      <ion-badge style=\"margin-top: -65%;z-index: 1000 !important;margin-left: 6%\" color=\"danger\">{{total ? total : cero}}</ion-badge>\n    </ion-fab-button>\n  </ion-fab> \n\n</ion-content>\n\n<app-tab *ngIf=\"home\"></app-tab>";
    /***/
  },

  /***/
  "./src/app/pages/inicio/inicio-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/inicio/inicio-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: InicioPageRoutingModule */

  /***/
  function srcAppPagesInicioInicioRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InicioPageRoutingModule", function () {
      return InicioPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _inicio_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./inicio.page */
    "./src/app/pages/inicio/inicio.page.ts");

    var routes = [{
      path: '',
      component: _inicio_page__WEBPACK_IMPORTED_MODULE_3__["InicioPage"]
    }];

    var InicioPageRoutingModule = function InicioPageRoutingModule() {
      _classCallCheck(this, InicioPageRoutingModule);
    };

    InicioPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], InicioPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/inicio/inicio.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/pages/inicio/inicio.module.ts ***!
    \***********************************************/

  /*! exports provided: InicioPageModule */

  /***/
  function srcAppPagesInicioInicioModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InicioPageModule", function () {
      return InicioPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _inicio_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./inicio-routing.module */
    "./src/app/pages/inicio/inicio-routing.module.ts");
    /* harmony import */


    var _inicio_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./inicio.page */
    "./src/app/pages/inicio/inicio.page.ts");
    /* harmony import */


    var src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/components/tab/tab.component */
    "./src/app/components/tab/tab.component.ts");

    var InicioPageModule = function InicioPageModule() {
      _classCallCheck(this, InicioPageModule);
    };

    InicioPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _inicio_routing_module__WEBPACK_IMPORTED_MODULE_5__["InicioPageRoutingModule"]],
      declarations: [_inicio_page__WEBPACK_IMPORTED_MODULE_6__["InicioPage"], src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__["TabComponent"]]
    })], InicioPageModule);
    /***/
  },

  /***/
  "./src/app/pages/inicio/inicio.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/pages/inicio/inicio.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesInicioInicioPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".start-btn {\n  width: 70px;\n  height: 70px;\n  margin-top: 40%;\n  margin-right: 10px;\n}\n\n.iconSize {\n  font-size: 50px;\n}\n\n.scroll {\n  overflow-x: scroll;\n  overflow-y: hidden;\n}\n\n.noticiaItem {\n  font-size: 14px;\n  background: white;\n  color: black;\n  padding: 3%;\n  text-transform: capitalize;\n  position: absolute;\n  width: 90%;\n  bottom: 10%;\n  margin-left: 3%;\n  margin-right: 3%;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n}\n\n.new {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  width: 35%;\n  margin-top: 10%;\n}\n\n.icono {\n  position: absolute;\n  font-size: 600%;\n  left: 0px;\n  right: 0px;\n  margin: 0px auto;\n  margin-top: 80%;\n  text-align: center;\n}\n\n.ico {\n  position: absolute;\n  left: 0px;\n  right: 0px;\n  margin: 0px auto;\n  margin-top: 80%;\n  text-align: center;\n  font-size: 800%;\n}\n\n.audio {\n  position: absolute;\n  font-size: 800%;\n  left: 0px;\n  right: 0px;\n  margin: 0px auto;\n  margin-top: 140%;\n  text-align: center;\n}\n\n.img {\n  display: block;\n  width: 27%;\n  margin: 0px auto;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 4%;\n}\n\n.start-btn {\n  width: 65px;\n  height: 65px;\n}\n\n.iconSize {\n  font-size: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yb2JpbnJvamFzL3dlYkFwcC9BcHAvRU1QQU5BREFTX0JBUi9zcmMvYXBwL3BhZ2VzL2luaWNpby9pbmljaW8ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9pbmljaW8vaW5pY2lvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDREo7O0FESUU7RUFDRSxlQUFBO0FDREo7O0FESUU7RUFDRyxrQkFBQTtFQUNBLGtCQUFBO0FDREw7O0FESUU7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUVBLDBDQUFBO0FDRko7O0FES0E7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FDRko7O0FES0E7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFFQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDSEo7O0FETUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDSEo7O0FETUE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFFQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ0pKOztBRE9BO0VBQ0ksY0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FDSko7O0FET0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0pKOztBRE9FO0VBQ0UsZUFBQTtBQ0pKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaW5pY2lvL2luaWNpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLnN0YXJ0LWJ0biB7XG4gICAgd2lkdGg6IDcwcHg7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIG1hcmdpbi10b3A6IDQwJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIH1cblxuICAuaWNvblNpemUge1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgfVxuXG4gIC5zY3JvbGx7XG4gICAgIG92ZXJmbG93LXg6c2Nyb2xsOyBcbiAgICAgb3ZlcmZsb3cteTpoaWRkZW47XG4gIH0gIFxuXG4gIC5ub3RpY2lhSXRlbXtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LDEpO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBwYWRkaW5nOiAzJTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBib3R0b206IDEwJTtcbiAgICBtYXJnaW4tbGVmdDogMyU7XG4gICAgbWFyZ2luLXJpZ2h0OiAzJTtcbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCAjY2NjY2NjNGQ7XG4gICAgYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG59XG5cbi5uZXd7XG4gICAgZGlzcGxheTpibG9jaztcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IDM1JTtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG59XG5cbi5pY29ub3tcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1zaXplOiA2MDAlO1xuICAgIC8vIGNvbG9yOiBibGFjaztcbiAgICBsZWZ0OiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbiAgICBtYXJnaW46IDBweCBhdXRvO1xuICAgIG1hcmdpbi10b3A6IDgwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxufVxuXG4uaWNve1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbiAgICBtYXJnaW46IDBweCBhdXRvO1xuICAgIG1hcmdpbi10b3A6IDgwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiA4MDAlO1xufVxuXG4uYXVkaW97XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGZvbnQtc2l6ZTogODAwJTtcbiAgICAvLyBjb2xvcjogYmxhY2s7XG4gICAgbGVmdDogMHB4O1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgbWFyZ2luOiAwcHggYXV0bztcbiAgICBtYXJnaW4tdG9wOiAxNDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmltZ3tcbiAgICBkaXNwbGF5OmJsb2NrO1xuICAgIHdpZHRoOiAyNyU7XG4gICAgbWFyZ2luOiAwcHggYXV0bztcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLXRvcDogNCU7XG59XG5cbi5zdGFydC1idG4ge1xuICAgIHdpZHRoOiA2NXB4O1xuICAgIGhlaWdodDogNjVweDtcbiAgfVxuXG4gIC5pY29uU2l6ZSB7XG4gICAgZm9udC1zaXplOiA0MHB4O1xuICB9XG5cbiAgXG4gIFxuICIsIi5zdGFydC1idG4ge1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBtYXJnaW4tdG9wOiA0MCU7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLmljb25TaXplIHtcbiAgZm9udC1zaXplOiA1MHB4O1xufVxuXG4uc2Nyb2xsIHtcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG59XG5cbi5ub3RpY2lhSXRlbSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGNvbG9yOiBibGFjaztcbiAgcGFkZGluZzogMyU7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA5MCU7XG4gIGJvdHRvbTogMTAlO1xuICBtYXJnaW4tbGVmdDogMyU7XG4gIG1hcmdpbi1yaWdodDogMyU7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cblxuLm5ldyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB3aWR0aDogMzUlO1xuICBtYXJnaW4tdG9wOiAxMCU7XG59XG5cbi5pY29ubyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiA2MDAlO1xuICBsZWZ0OiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIG1hcmdpbi10b3A6IDgwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaWNvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIG1hcmdpbi10b3A6IDgwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDgwMCU7XG59XG5cbi5hdWRpbyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiA4MDAlO1xuICBsZWZ0OiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIG1hcmdpbi10b3A6IDE0MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmltZyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMjclO1xuICBtYXJnaW46IDBweCBhdXRvO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiA0JTtcbn1cblxuLnN0YXJ0LWJ0biB7XG4gIHdpZHRoOiA2NXB4O1xuICBoZWlnaHQ6IDY1cHg7XG59XG5cbi5pY29uU2l6ZSB7XG4gIGZvbnQtc2l6ZTogNDBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/inicio/inicio.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/pages/inicio/inicio.page.ts ***!
    \*********************************************/

  /*! exports provided: InicioPage */

  /***/
  function srcAppPagesInicioInicioPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InicioPage", function () {
      return InicioPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/service/api-services.service */
    "./src/app/service/api-services.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _modal_home_modal_home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../modal-home/modal-home.page */
    "./src/app/pages/modal-home/modal-home.page.ts");
    /* harmony import */


    var _pedido_pedido_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../pedido/pedido.page */
    "./src/app/pages/pedido/pedido.page.ts");

    var InicioPage = /*#__PURE__*/function () {
      function InicioPage(modalController, plt, servicios, loadingController, router) {
        _classCallCheck(this, InicioPage);

        this.modalController = modalController;
        this.plt = plt;
        this.servicios = servicios;
        this.loadingController = loadingController;
        this.router = router;
        this.home = [];
        this.verPedido = [];
        this.CarritoTotal = [];
        this.total = 0;
        this.cero = 0;
        this.precio = [{
          precio: 10000
        }, {
          precio: 8000
        }, {
          precio: 11000
        }, {
          precio: 9000
        }, {
          precio: 10000
        }, {
          precio: 12000
        }, {
          precio: 7000
        }, {
          precio: 9000
        }, {
          precio: 8000
        }, {
          precio: 10000
        }, {
          precio: 9000
        }, {
          precio: 10000
        }, {
          precio: 11000
        }, {
          precio: 12000
        }, {
          precio: 9000
        }, {
          precio: 6000
        }, {
          precio: 9000
        }, {
          precio: 8000
        }, {
          precio: 11000
        }, {
          precio: 10000
        }, {
          precio: 8000
        }];
      }

      _createClass(InicioPage, [{
        key: "listadoHome",
        value: function listadoHome() {
          var _this = this;

          this.Presentloading();
          this.servicios.getHome().subscribe(function (data) {
            _this.home = data;
            console.log(_this.home);

            _this.salirLoading();
          });
        }
      }, {
        key: "Presentloading",
        value: function Presentloading() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var loading;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingController.create({
                      message: 'Cargando...'
                    });

                  case 2:
                    loading = _context.sent;
                    _context.next = 5;
                    return loading.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "salirLoading",
        value: function salirLoading() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.dismiss();

                  case 2:
                    return _context2.abrupt("return", _context2.sent);

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.total = 0;
          this.listadoHome();
        }
      }, {
        key: "agregarCarrito",
        value: function agregarCarrito(id, precio, rendered, imagen, indice) {
          this.CarritoTotal.push({
            id: id,
            precio: precio,
            titulo: rendered,
            imagen: imagen,
            cantidad: 1
          });
          this.home[indice].cantidad = 1;
          this.actualizarcarrito();
          this.total += 1;

          if (this.total == 0) {
            this.CarritoTotal = [];
          }
        }
      }, {
        key: "pedido",
        value: function pedido() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this2 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    if (this.total == 0) {
                      this.CarritoTotal = [];
                    }

                    _context3.next = 3;
                    return this.modalController.create({
                      component: _pedido_pedido_page__WEBPACK_IMPORTED_MODULE_6__["PedidoPage"],
                      cssClass: 'my-custom-modal-css',
                      componentProps: {
                        'carrito': this.CarritoTotal,
                        'total': this.total
                      }
                    });

                  case 3:
                    modal = _context3.sent;
                    modal.onDidDismiss().then(function (data) {
                      var carrito = data['data'].carrito;

                      if (carrito == true) {
                        _this2.home = [];

                        _this2.listadoHome();

                        _this2.CarritoTotal = [];
                        _this2.total = 0;
                      }
                    });
                    _context3.next = 7;
                    return modal.present();

                  case 7:
                    return _context3.abrupt("return", _context3.sent);

                  case 8:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "restar",
        value: function restar(indice, cantidad, id) {
          this.total = this.total - 1;
          this.home[indice].cantidad = cantidad - 1;
          var posicion = this.CarritoTotal.findIndex(function (x) {
            return x.id == id;
          });
          this.CarritoTotal[posicion].cantidad--;
          this.actualizarcarrito();

          if (this.total == 0) {
            this.CarritoTotal = [];
          }
        }
      }, {
        key: "actualizarcarrito",
        value: function actualizarcarrito() {
          var _iterator = _createForOfIteratorHelper(this.CarritoTotal),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var pedido = _step.value;
              this.verCantidadCarrito = pedido.cantidad;
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
      }, {
        key: "sumar",
        value: function sumar(id, indice, cantidad) {
          this.total = this.total + 1;
          this.home[indice].cantidad = cantidad + 1;
          var posicion = this.CarritoTotal.findIndex(function (x) {
            return x.id == id;
          });
          this.CarritoTotal[posicion].cantidad++;
          this.actualizarcarrito();
        }
      }, {
        key: "verificarCantidad",
        value: function verificarCantidad(indice) {
          if (this.home[indice].cantidad > 0) {
            return true;
          } else {
            return false;
          }
        }
      }, {
        key: "verHome",
        value: function verHome(item) {
          var navigationExtras = {
            state: {
              itemHome: item
            }
          };
          this.router.navigate(['/ver-home'], navigationExtras);
        }
      }, {
        key: "presentModal",
        value: function presentModal() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var modal;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.modalController.create({
                      component: _modal_home_modal_home_page__WEBPACK_IMPORTED_MODULE_5__["ModalHomePage"],
                      cssClass: 'my-custom-modal-css'
                    });

                  case 2:
                    modal = _context4.sent;
                    _context4.next = 5;
                    return modal.present();

                  case 5:
                    return _context4.abrupt("return", _context4.sent);

                  case 6:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          var _this3 = this;

          setTimeout(function () {
            _this3.listadoHome();

            event.target.complete();
          }, 2000);
        }
      }]);

      return InicioPage;
    }();

    InicioPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__["ApiServicesService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    InicioPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-inicio',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./inicio.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/inicio/inicio.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./inicio.page.scss */
      "./src/app/pages/inicio/inicio.page.scss"))["default"]]
    })], InicioPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-inicio-inicio-module-es5.js.map