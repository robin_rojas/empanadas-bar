function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modal-home-modal-home-module"], {
  /***/
  "./src/app/pages/modal-home/modal-home-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/modal-home/modal-home-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: ModalHomePageRoutingModule */

  /***/
  function srcAppPagesModalHomeModalHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ModalHomePageRoutingModule", function () {
      return ModalHomePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _modal_home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./modal-home.page */
    "./src/app/pages/modal-home/modal-home.page.ts");

    var routes = [{
      path: '',
      component: _modal_home_page__WEBPACK_IMPORTED_MODULE_3__["ModalHomePage"]
    }];

    var ModalHomePageRoutingModule = function ModalHomePageRoutingModule() {
      _classCallCheck(this, ModalHomePageRoutingModule);
    };

    ModalHomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ModalHomePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/modal-home/modal-home.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/modal-home/modal-home.module.ts ***!
    \*******************************************************/

  /*! exports provided: ModalHomePageModule */

  /***/
  function srcAppPagesModalHomeModalHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ModalHomePageModule", function () {
      return ModalHomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _modal_home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./modal-home-routing.module */
    "./src/app/pages/modal-home/modal-home-routing.module.ts");
    /* harmony import */


    var _modal_home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./modal-home.page */
    "./src/app/pages/modal-home/modal-home.page.ts");

    var ModalHomePageModule = function ModalHomePageModule() {
      _classCallCheck(this, ModalHomePageModule);
    };

    ModalHomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _modal_home_routing_module__WEBPACK_IMPORTED_MODULE_5__["ModalHomePageRoutingModule"]],
      declarations: [_modal_home_page__WEBPACK_IMPORTED_MODULE_6__["ModalHomePage"]]
    })], ModalHomePageModule);
    /***/
  },

  /***/
  "./src/app/service/api-services.service.ts":
  /*!*************************************************!*\
    !*** ./src/app/service/api-services.service.ts ***!
    \*************************************************/

  /*! exports provided: ApiServicesService */

  /***/
  function srcAppServiceApiServicesServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApiServicesService", function () {
      return ApiServicesService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var ApiServicesService = /*#__PURE__*/function () {
      function ApiServicesService(http) {
        _classCallCheck(this, ApiServicesService);

        this.http = http;
        this.cabecera = new Headers();
        this.cabecera.append('Access-Control-Allow-Origin', '*');
        this.cabecera.append("Cache-Control", "no-cache");
        this.cabecera.append("Access-Control-Allow-Methods", 'GET,POST,OPTIONS,DELETE,PUT');
        this.cabecera.append("Access-Control-Allow-Headers", "gid, filename, Origin, X - Requested - With, Content - Type, Accept, Authorization");
      }

      _createClass(ApiServicesService, [{
        key: "getHome",
        value: function getHome() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=28&tags=27', {
            headers: this.cabecera
          });
        }
      }, {
        key: "enviarComentario",
        value: function enviarComentario(params) {
          return this.http.post('https://mpglobal.info/servicio.php', params, {
            headers: this.cabecera
          });
        }
      }, {
        key: "getImagenes",
        value: function getImagenes() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=30&tags=29', {
            headers: this.cabecera
          });
        }
      }, {
        key: "getContacto",
        value: function getContacto() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=31&tags=32', {
            headers: this.cabecera
          });
        }
      }, {
        key: "getModal",
        value: function getModal() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=33&tags=34', {
            headers: this.cabecera
          });
        } // Envio de Pedido

      }, {
        key: "enviarPedido",
        value: function enviarPedido(params) {
          return this.http.post('https://empanadas.bar/servicios_empanadas_bar/servicios_envio_pedido.php', params, {
            headers: this.cabecera
          });
        }
      }]);

      return ApiServicesService;
    }();

    ApiServicesService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    ApiServicesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], ApiServicesService);
    /***/
  }
}]);
//# sourceMappingURL=pages-modal-home-modal-home-module-es5.js.map