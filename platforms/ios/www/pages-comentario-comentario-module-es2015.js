(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-comentario-comentario-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/comentario/comentario.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/comentario/comentario.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" routerLink=\"/inicio\"></ion-back-button>\n    </ion-buttons>\n    <ion-title style=\"font-size: 13px;\"><ion-icon style=\"font-size: 30px;\" name=\"mail-open\"></ion-icon>\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid>\n\n    <ion-label style=\"display: flex;justify-content: center;margin-top: 5%;padding:5%;text-align:justify\">Deja tu comentario sobre EMPANADAS BAR \n      la que ofrece vital información.\n    </ion-label>\n\n    <ion-item style=\"margin-top: 4%;\">\n      <ion-label position=\"stacked\">Nombres <ion-text color=\"danger\">*</ion-text></ion-label>\n      <ion-input [(ngModel)]=\"nombres\" type=\"text\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Teléfono <ion-text color=\"danger\">*</ion-text></ion-label>\n      <ion-input [(ngModel)]=\"telefono\" type=\"number\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Correo electronico <ion-text color=\"danger\">*</ion-text></ion-label>\n      <ion-input [(ngModel)]=\"email\" type=\"email\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Mensaje <ion-text color=\"danger\">*</ion-text></ion-label>\n      <ion-textarea [(ngModel)]=\"mensaje\" type=\"text\"></ion-textarea>\n    </ion-item>\n\n    <section class=\"full-width\" style=\"margin-top: 10%;\">\n      <ion-button (click)=\"EnviarDatos()\" expand=\"full\" color=\"success\">Enviar <ion-icon style=\"margin-left: 3%;\" name=\"paper-plane\"></ion-icon></ion-button>\n    </section>\n\n  </ion-grid>\n\n</ion-content>\n\n<app-tab></app-tab>\n");

/***/ }),

/***/ "./src/app/pages/comentario/comentario-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/comentario/comentario-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: ComentarioPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComentarioPageRoutingModule", function() { return ComentarioPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _comentario_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./comentario.page */ "./src/app/pages/comentario/comentario.page.ts");




const routes = [
    {
        path: '',
        component: _comentario_page__WEBPACK_IMPORTED_MODULE_3__["ComentarioPage"]
    }
];
let ComentarioPageRoutingModule = class ComentarioPageRoutingModule {
};
ComentarioPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ComentarioPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/comentario/comentario.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/comentario/comentario.module.ts ***!
  \*******************************************************/
/*! exports provided: ComentarioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComentarioPageModule", function() { return ComentarioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _comentario_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./comentario-routing.module */ "./src/app/pages/comentario/comentario-routing.module.ts");
/* harmony import */ var _comentario_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./comentario.page */ "./src/app/pages/comentario/comentario.page.ts");
/* harmony import */ var src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/tab/tab.component */ "./src/app/components/tab/tab.component.ts");








let ComentarioPageModule = class ComentarioPageModule {
};
ComentarioPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _comentario_routing_module__WEBPACK_IMPORTED_MODULE_5__["ComentarioPageRoutingModule"]
        ],
        declarations: [_comentario_page__WEBPACK_IMPORTED_MODULE_6__["ComentarioPage"], src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__["TabComponent"]]
    })
], ComentarioPageModule);



/***/ }),

/***/ "./src/app/pages/comentario/comentario.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/comentario/comentario.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbWVudGFyaW8vY29tZW50YXJpby5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/comentario/comentario.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/comentario/comentario.page.ts ***!
  \*****************************************************/
/*! exports provided: ComentarioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComentarioPage", function() { return ComentarioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/api-services.service */ "./src/app/service/api-services.service.ts");




let ComentarioPage = class ComentarioPage {
    constructor(servicios, toastController) {
        this.servicios = servicios;
        this.toastController = toastController;
    }
    ngOnInit() {
    }
    showToastRegistroMensaje(text) {
        this.toast = this.toastController.create({
            message: text,
            duration: 2000,
            position: 'top',
        }).then((toastData) => {
            toastData.present();
        });
    }
    EnviarDatos() {
        if (!this.nombres) {
            this.showToastRegistroMensaje('Porfavor llenar el campo nombre');
        }
        else if (!this.telefono) {
            this.showToastRegistroMensaje('Porfavor llenar el campo telefono');
        }
        else if (!this.email) {
            this.showToastRegistroMensaje('Porfavor llenar el campo email');
        }
        else if (!this.mensaje) {
            this.showToastRegistroMensaje('Porfavor llenar el campo mensaje');
        }
        else {
            let params = {
                nombres: this.nombres,
                telefono: this.telefono,
                email: this.email,
                mensaje: this.mensaje
            };
            console.log(params);
            this.servicios.enviarComentario(params)
                .subscribe((data) => {
                if (data['logger'] == true) {
                    this.showToast(data['msg']);
                }
                else {
                    this.showToast(data['msg']);
                    console.log(data);
                }
                this.nombres = "";
                this.telefono = "";
                this.email = "";
                this.mensaje = "";
            });
        }
    }
    showToast(text) {
        this.toast = this.toastController.create({
            message: text,
            duration: 2000,
            position: 'top',
        }).then((toastData) => {
            toastData.present();
        });
    }
    HideToast() {
        this.toast = this.toastController.dismiss();
    }
};
ComentarioPage.ctorParameters = () => [
    { type: src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__["ApiServicesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ComentarioPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-comentario',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./comentario.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/comentario/comentario.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./comentario.page.scss */ "./src/app/pages/comentario/comentario.page.scss")).default]
    })
], ComentarioPage);



/***/ }),

/***/ "./src/app/service/api-services.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/api-services.service.ts ***!
  \*************************************************/
/*! exports provided: ApiServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiServicesService", function() { return ApiServicesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



let ApiServicesService = class ApiServicesService {
    constructor(http) {
        this.http = http;
        this.cabecera = new Headers();
        this.cabecera.append('Access-Control-Allow-Origin', '*');
        this.cabecera.append("Cache-Control", "no-cache");
        this.cabecera.append("Access-Control-Allow-Methods", 'GET,POST,OPTIONS,DELETE,PUT');
        this.cabecera.append("Access-Control-Allow-Headers", "gid, filename, Origin, X - Requested - With, Content - Type, Accept, Authorization");
    }
    getHome() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=28&tags=27', { headers: this.cabecera });
    }
    enviarComentario(params) {
        return this.http.post('https://mpglobal.info/servicio.php', params, { headers: this.cabecera });
    }
    getImagenes() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=30&tags=29', { headers: this.cabecera });
    }
    getContacto() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=31&tags=32', { headers: this.cabecera });
    }
    getModal() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=33&tags=34', { headers: this.cabecera });
    }
    // Envio de Pedido
    enviarPedido(params) {
        return this.http.post('https://empanadas.bar/servicios_empanadas_bar/servicios_envio_pedido.php', params, { headers: this.cabecera });
    }
};
ApiServicesService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiServicesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiServicesService);



/***/ })

}]);
//# sourceMappingURL=pages-comentario-comentario-module-es2015.js.map