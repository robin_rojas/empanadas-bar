(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-galeria-galeria-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/galeria/galeria.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/galeria/galeria.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n\n<ion-content>\n\n  <ion-col *ngFor=\"let item of imagenes;let i = index\">\n    <img [src]=\"item.featured_image.size_large\">    \n    <h3 style=\"text-align: center;\" [innerHTML]=\"item.title.rendered\"></h3> \n    <p class=\"textLista\" style=\"text-align:justify;font-size:15px;padding:5%\" [innerHTML]=\"item.content.rendered\"></p>  \n  </ion-col>\n\n</ion-content>\n\n<app-tab></app-tab> \n");

/***/ }),

/***/ "./src/app/pages/galeria/galeria-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/galeria/galeria-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: GaleriaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleriaPageRoutingModule", function() { return GaleriaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _galeria_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./galeria.page */ "./src/app/pages/galeria/galeria.page.ts");




const routes = [
    {
        path: '',
        component: _galeria_page__WEBPACK_IMPORTED_MODULE_3__["GaleriaPage"]
    }
];
let GaleriaPageRoutingModule = class GaleriaPageRoutingModule {
};
GaleriaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GaleriaPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/galeria/galeria.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/galeria/galeria.module.ts ***!
  \*************************************************/
/*! exports provided: GaleriaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleriaPageModule", function() { return GaleriaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _galeria_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./galeria-routing.module */ "./src/app/pages/galeria/galeria-routing.module.ts");
/* harmony import */ var _galeria_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./galeria.page */ "./src/app/pages/galeria/galeria.page.ts");
/* harmony import */ var src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/tab/tab.component */ "./src/app/components/tab/tab.component.ts");








let GaleriaPageModule = class GaleriaPageModule {
};
GaleriaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _galeria_routing_module__WEBPACK_IMPORTED_MODULE_5__["GaleriaPageRoutingModule"]
        ],
        declarations: [_galeria_page__WEBPACK_IMPORTED_MODULE_6__["GaleriaPage"], src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__["TabComponent"]]
    })
], GaleriaPageModule);



/***/ }),

/***/ "./src/app/pages/galeria/galeria.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/galeria/galeria.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2dhbGVyaWEvZ2FsZXJpYS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/galeria/galeria.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/galeria/galeria.page.ts ***!
  \***********************************************/
/*! exports provided: GaleriaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleriaPage", function() { return GaleriaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/api-services.service */ "./src/app/service/api-services.service.ts");




let GaleriaPage = class GaleriaPage {
    constructor(servicios, loadingController) {
        this.servicios = servicios;
        this.loadingController = loadingController;
        this.imagenes = [];
    }
    ngOnInit() {
        this.listadoImagenes();
    }
    listadoImagenes() {
        this.Presentloading();
        this.servicios.getImagenes()
            .subscribe((data) => {
            this.imagenes = data;
            console.log(this.imagenes);
            this.salirLoading();
        });
    }
    Presentloading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
                duration: 1000,
            });
            yield loading.present();
        });
    }
    salirLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield this.loadingController.dismiss();
        });
    }
};
GaleriaPage.ctorParameters = () => [
    { type: src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__["ApiServicesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
GaleriaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-galeria',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./galeria.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/galeria/galeria.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./galeria.page.scss */ "./src/app/pages/galeria/galeria.page.scss")).default]
    })
], GaleriaPage);



/***/ }),

/***/ "./src/app/service/api-services.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/api-services.service.ts ***!
  \*************************************************/
/*! exports provided: ApiServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiServicesService", function() { return ApiServicesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



let ApiServicesService = class ApiServicesService {
    constructor(http) {
        this.http = http;
        this.cabecera = new Headers();
        this.cabecera.append('Access-Control-Allow-Origin', '*');
        this.cabecera.append("Cache-Control", "no-cache");
        this.cabecera.append("Access-Control-Allow-Methods", 'GET,POST,OPTIONS,DELETE,PUT');
        this.cabecera.append("Access-Control-Allow-Headers", "gid, filename, Origin, X - Requested - With, Content - Type, Accept, Authorization");
    }
    getHome() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=28&tags=27', { headers: this.cabecera });
    }
    enviarComentario(params) {
        return this.http.post('https://mpglobal.info/servicio.php', params, { headers: this.cabecera });
    }
    getImagenes() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=30&tags=29', { headers: this.cabecera });
    }
    getContacto() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=31&tags=32', { headers: this.cabecera });
    }
    getModal() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=33&tags=34', { headers: this.cabecera });
    }
    // Envio de Pedido
    enviarPedido(params) {
        return this.http.post('https://empanadas.bar/servicios_empanadas_bar/servicios_envio_pedido.php', params, { headers: this.cabecera });
    }
};
ApiServicesService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiServicesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiServicesService);



/***/ })

}]);
//# sourceMappingURL=pages-galeria-galeria-module-es2015.js.map