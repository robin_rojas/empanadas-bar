import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ApiServicesService } from 'src/app/service/api-services.service';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.page.html',
  styleUrls: ['./comentario.page.scss'],
})
export class ComentarioPage implements OnInit {

  toast;
  nombres: any;
  telefono: any;
  email: any;
  mensaje: any;

  constructor(public servicios: ApiServicesService,
    public toastController: ToastController) { }

  ngOnInit() {
  }

  showToastRegistroMensaje(text) {
    this.toast = this.toastController.create({
      message: text,
      duration: 2000,
      position: 'top',
    }).then((toastData)=>{
      toastData.present();
    });
  }

  EnviarDatos(){
    if (!this.nombres)  {
      this.showToastRegistroMensaje('Porfavor llenar el campo nombre');
    } else if (!this.telefono) {
      this.showToastRegistroMensaje('Porfavor llenar el campo telefono');
    } else if (!this.email) {
      this.showToastRegistroMensaje('Porfavor llenar el campo email');
    } else if (!this.mensaje) {
      this.showToastRegistroMensaje('Porfavor llenar el campo mensaje');
    } else {
      let params = {
        nombres: this.nombres,
        telefono: this.telefono,
        email: this.email,
        mensaje: this.mensaje
      }
      console.log(params);
      this.servicios.enviarComentario(params)
      .subscribe( (data) => {
        if (data['logger'] == true) {
          this.showToast(data['msg']); 
        } else {
          this.showToast(data['msg']); 
          console.log(data);
        }
        this.nombres = "";
        this.telefono = "";
        this.email = "";
        this.mensaje = "";
      });
    }
  }

  showToast(text) {
    this.toast = this.toastController.create({
      message: text,
      duration: 2000,
      position: 'top',
    }).then((toastData)=>{
      toastData.present();
    });
  }

  HideToast(){
    this.toast = this.toastController.dismiss();
  }


}
