import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ApiServicesService } from 'src/app/service/api-services.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {

  contact: any = [];

  constructor(public servicios: ApiServicesService,
    private iab: InAppBrowser,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.listadoContact();
  }

  listadoContact(){
    this.Presentloading();
    this.servicios.getContacto()
    .subscribe( (data) => {
      this.contact = data;
      console.log(this.contact);
      this.salirLoading();
    });
  }

  abrirFacebook(){
    const options: InAppBrowserOptions = {
      closebuttoncaption: 'ATRÁS/BACK',
      fullscreen: "yes", 
      closebuttoncolor: '#F2F7F7',
      footer: 'no',
      mediaPlaybackRequiresUserAction: 'yes',
      location : 'no',
      hidden : 'no', 
      hideurlbar:'yes',
    }
    this.iab.create('https://www.facebook.com/ENTRENAN/', '_system', options);
  }

  async Presentloading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 1000,
    });
    await loading.present();
  }   

  async salirLoading(){
    return await this.loadingController.dismiss();
  } 

}
