import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ApiServicesService } from 'src/app/service/api-services.service';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {

  imagenes: any = []; 

  constructor(public servicios: ApiServicesService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.listadoImagenes();
  }

  listadoImagenes(){
    this.Presentloading();
    this.servicios.getImagenes()
    .subscribe( (data) =>{
      this.imagenes = data;
      console.log(this.imagenes);
      this.salirLoading();
    }); 
  }

  async Presentloading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 1000,
    });
    await loading.present();
  }   

  async salirLoading(){
    return await this.loadingController.dismiss();
  } 

}
