import { Component, OnInit } from '@angular/core';
import { ModalController, Platform, LoadingController } from '@ionic/angular';
import { ApiServicesService } from 'src/app/service/api-services.service';
import { Router, NavigationExtras } from '@angular/router';
import { ModalHomePage } from '../modal-home/modal-home.page';
import { PedidoPage } from '../pedido/pedido.page';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  
  ios: any;
  android: any;
  home: any = [];
  Verificador: any;
  verPedido: any = [];
  verCantidadCarrito: any;
  CarritoTotal: any = [];
  total: any = 0;
  totalSuma: any;
  cero = 0;
  precio = [
    {
      precio: 10000,
    },
    {
      precio: 8000,
    },
    {
      precio: 11000,
    },
    {
      precio: 9000,
    },
    {
      precio: 10000,
    },
    {
      precio: 12000,
    },
    {
      precio: 7000,
    },
    {
      precio: 9000,
    },
    {
      precio: 8000,
    },
    {
      precio: 10000,
    },
    {
      precio: 9000,
    },
    {
      precio: 10000,
    },
    {
      precio: 11000,
    },
    {
      precio: 12000,
    },
    {
      precio: 9000,
    },
    {
      precio: 6000,
    },
    {
      precio: 9000,
    },
    {
      precio: 8000,
    },
    {
      precio: 11000,
    },
    {
      precio: 10000,
    },
    {
      precio: 8000,
    }
  ]

  constructor(public modalController: ModalController,
              public plt: Platform,
              public servicios: ApiServicesService,
              public loadingController: LoadingController,
              public router: Router) {
  }

  listadoHome(){
    this.Presentloading();
    this.servicios.getHome()
    .subscribe( (data) => {
      this.home = data;
      console.log(this.home);
      this.salirLoading();
    });
  }

  async Presentloading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present();
  }   

  async salirLoading(){
    return await this.loadingController.dismiss();
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
      this.total = 0;
      this.listadoHome();    
  }

  agregarCarrito(id,precio,rendered,imagen,indice){
      this.CarritoTotal.push({
        id: id,
        precio: precio,
        titulo: rendered,
        imagen: imagen,
        cantidad : 1
    });
    this.home[indice].cantidad = 1;
    this.actualizarcarrito();  
    this.total += 1;
    if(this.total == 0){
      this.CarritoTotal = [];
    }
  }

  async pedido() {
    if(this.total == 0){
      this.CarritoTotal = [];
    }
      const modal = await this.modalController.create({
        component: PedidoPage,
        cssClass: 'my-custom-modal-css',
        componentProps: {
          'carrito': this.CarritoTotal,
          'total': this.total,
        }
      });
      modal.onDidDismiss()
        .then((data) => {
          const carrito = data['data'].carrito;
          if(carrito == true){
            this.home = [];
            this.listadoHome();
            this.CarritoTotal = [];
            this.total = 0;
          }
      });
      return await modal.present();
  }

  restar(indice,cantidad,id){
    this.total = this.total - 1;
    this.home[indice].cantidad = cantidad - 1;
    let posicion = this.CarritoTotal.findIndex(
      x => x.id == id
    );
    this.CarritoTotal[posicion].cantidad --;
    this.actualizarcarrito();
    if(this.total == 0){
      this.CarritoTotal = [];
    }
  }

  actualizarcarrito(){
    for (let pedido of this.CarritoTotal){
      this.verCantidadCarrito = pedido.cantidad;
    }
  }

  sumar(id,indice,cantidad){
      this.total = this.total + 1;
      this.home[indice].cantidad = cantidad + 1;
      let posicion = this.CarritoTotal.findIndex(
        x => x.id == id
      );
      this.CarritoTotal[posicion].cantidad ++;
      this.actualizarcarrito();
  }

  verificarCantidad(indice){
    if(this.home[indice].cantidad > 0){
      return true
    } else {
      return false
    }
  } 

  verHome(item){
    let navigationExtras: NavigationExtras = {
      state: {
        itemHome: item
      }
    }
    this.router.navigate(['/ver-home'],navigationExtras); 
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalHomePage,
      cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.listadoHome();
      event.target.complete();
    }, 2000);
  }

}
