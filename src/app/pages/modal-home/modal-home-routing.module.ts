import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalHomePage } from './modal-home.page';

const routes: Routes = [
  {
    path: '',
    component: ModalHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalHomePageRoutingModule {}
