import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalHomePageRoutingModule } from './modal-home-routing.module';

import { ModalHomePage } from './modal-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalHomePageRoutingModule
  ],
  declarations: [ModalHomePage]
})
export class ModalHomePageModule {}
