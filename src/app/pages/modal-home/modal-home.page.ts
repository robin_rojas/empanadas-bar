import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, LoadingController } from '@ionic/angular';
import { ApiServicesService } from 'src/app/service/api-services.service';

@Component({
  selector: 'app-modal-home',
  templateUrl: './modal-home.page.html',
  styleUrls: ['./modal-home.page.scss'],
})
export class ModalHomePage implements OnInit {

  modal: any = [];
  isLoading = false;

  constructor(public modalController: ModalController,
              public servicios: ApiServicesService,
              public loadingController: LoadingController,
              public router: Router) { }

  ngOnInit() {
    this.listadoModal();
  }

  listadoModal(){
    this.Presentloading();
    this.servicios.getModal()
    .subscribe( (data) => {
      this.modal = data;
      console.log(this.modal);
      this.salirLoading();
    });
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  async Presentloading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 1000,
    });
    await loading.present();
  }  
  
  async salirLoading() {
    if (this.isLoading) {
      this.isLoading = false;
      return await this.loadingController.dismiss();
    }
    return null;
  }

  // async salirLoading(){
  //   return await this.loadingController.dismiss();
  // } 

  EnviarComentario(){
    this.dismiss();
    this.router.navigate(['/comentario']); 
  }

}
