import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ModalController, NavParams, ToastController } from '@ionic/angular';
import { ApiServicesService } from 'src/app/service/api-services.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {

  toast;
  verPedido: any = [];
  ver: any;
  cantidad: any;
  precio: any;
  PrecioTotal: any = 0;
  TotalPrecio: any = 0;
  total: number;
  nombres: any;
  direccion: any;
  telefono: any;
  email: any;
  nota: any;
  Home: any = [];

  itemTotal: number = 0;

  constructor(public router: Router,
              public modalCtrl: ModalController,
              navParams: NavParams,
              public alertController: AlertController,
              public toastController: ToastController,
              public servicios: ApiServicesService,
              public loadingController: LoadingController) { 
                this.sumatoria(navParams);    
  }

  sumatoria(navParams){
    this.total = navParams.get('total');
    this.verPedido = navParams.get('carrito');
    for (let pedido of this.verPedido){
      // this.PrecioTotal += pedido.precio*pedido.cantidad;
      this.TotalPrecio += pedido.precio*pedido.cantidad;
      this.PrecioTotal = this.TotalPrecio.toLocaleString('de-DE');
    }
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    if(this.total == 0){
      this.verPedido = [];
      this.PrecioTotal = 0;
    }
  }

  limpiarProuctos(){
    this.verPedido = [];
    this.total = 0;
  }

  VolverPedido(){
    document.getElementById("listado").style.display = "block";
    document.getElementById("formulario").style.display = "none";
  }

  enviarProductos(){
    document.getElementById("listado").style.display = "none";
    document.getElementById("formulario").style.display = "block";
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'carrito': true
    });
  }

  cerrar(){
    if(this.verPedido.length == 0){
      this.modalCtrl.dismiss({
        'carrito': true
      });
    } else if(this.verPedido.length > 0) {
      this.modalCtrl.dismiss({
        'carrito': false
      });
    }
  }

  agregarAlCarrrito(){
    this.dismiss();  
  }

  async vaciarProducto() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Espera',
      message: '<strong>Esta seguro que desea eliminar tus productos</strong>',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'SI',
          handler: () => {
            this.limpiarProuctos();
          }
        }
      ]
    });
    await alert.present();
  }

  showToastRegistroMensaje(text) {
    this.toast = this.toastController.create({
      message: text,
      duration: 2000,
      position: 'top',
    }).then((toastData)=>{
      toastData.present();
    });
  }

  EnviarPedido(){
    if (!this.nombres)  {
      this.showToastRegistroMensaje('Porfavor llenar el campo nombres');
    } else if (!this.direccion) {
      this.showToastRegistroMensaje('Porfavor llenar el campo dirección');
    } else if (!this.telefono) {
      this.showToastRegistroMensaje('Porfavor llenar el campo telefono');
    } else if (!this.email) {
      this.showToastRegistroMensaje('Porfavor llenar el campo email');
    } else if (!this.nota) {
      this.showToastRegistroMensaje('Porfavor llenar el campo nota');
    } else {
      let params = {
        nombres: this.nombres,
        direccion: this.direccion,
        telefono: this.telefono,
        email: this.email,
        nota: this.nota,
        precioTotalRecetas: this.PrecioTotal,
        recetas: this.verPedido
      }
      this.Presentloading();
      this.servicios.enviarPedido(params)
      .subscribe( (data) => {
        if (data['logger'] == true) {
          this.showToast(data['msg']); 
          this.limpiarProuctos();
          this.salirLoading();
        } else {
          this.showToast(data['msg']); 
        }
          this.nombres = "";
          this.direccion = "";
          this.telefono = "";
          this.email = "";
          this.nota = "";
      });
    }
  } 

  async Presentloading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present();
  }   

  async salirLoading(){
    return await this.loadingController.dismiss();
  } 

  showToast(text) {
    this.toast = this.toastController.create({
      message: text,
      duration: 2000,
      position: 'top',
    }).then((toastData)=>{
      toastData.present();
    });
  }

  HideToast(){
    this.toast = this.toastController.dismiss();
  }

}
