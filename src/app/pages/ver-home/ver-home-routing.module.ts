import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerHomePage } from './ver-home.page';

const routes: Routes = [
  {
    path: '',
    component: VerHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerHomePageRoutingModule {}
