import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerHomePageRoutingModule } from './ver-home-routing.module';

import { VerHomePage } from './ver-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerHomePageRoutingModule
  ],
  declarations: [VerHomePage]
})
export class VerHomePageModule {}
