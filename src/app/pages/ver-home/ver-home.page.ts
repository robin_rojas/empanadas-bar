import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ver-home',
  templateUrl: './ver-home.page.html',
  styleUrls: ['./ver-home.page.scss'],
})
export class VerHomePage implements OnInit {

  verHome: any;

  constructor(public router: Router) {
    if(this.router.getCurrentNavigation().extras.state){
      this.verHome = this.router.getCurrentNavigation().extras.state.itemHome;
      console.log(this.verHome);
    } 
   }

  ngOnInit() {
  }

}
