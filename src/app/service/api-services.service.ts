import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  cabecera;

  constructor(private http: HttpClient) { 
    this.cabecera = new Headers();
    this.cabecera.append('Access-Control-Allow-Origin','*');
    this.cabecera.append("Cache-Control", "no-cache");
    this.cabecera.append("Access-Control-Allow-Methods", 'GET,POST,OPTIONS,DELETE,PUT');
    this.cabecera.append("Access-Control-Allow-Headers", "gid, filename, Origin, X - Requested - With, Content - Type, Accept, Authorization");
  }

  getHome(){
    return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=28&tags=27',{ headers: this.cabecera });
  }

  enviarComentario(params){
    return this.http.post('https://mpglobal.info/servicio.php',params, { headers: this.cabecera });
  }

  getImagenes(){
    return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=30&tags=29',{ headers: this.cabecera });
  }

  getContacto(){
    return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=31&tags=32',{ headers: this.cabecera });
  }

  getModal(){
    return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=33&tags=34',{ headers: this.cabecera });
  }

  // Envio de Pedido
  enviarPedido(params){
    return this.http.post('https://empanadas.bar/servicios_empanadas_bar/servicios_envio_pedido.php',params, { headers: this.cabecera });
  }

}
