function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-inicio-inicio-module~pages-pedido-pedido-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pedido/pedido.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pedido/pedido.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesPedidoPedidoPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-header>\n  <ion-toolbar>\n    <ion-row>\n      <ion-col>\n        <ion-icon *ngIf=\"!total == 0\" (click)=\"cerrar()\" style=\"font-size: 40px;color:#ed455f;margin-left: 2%;\" name=\"close-circle-outline\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-title style=\"font-size: 14px\">CARRITO DE COMPRAS</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen>\n  <ion-list id=\"listado\">\n    <ion-list-header *ngIf=\"!total == 0\" style=\"font-size: 17px;\">\n      <h5>Listado de Empanadas agregadas.</h5>\n    </ion-list-header>\n\n    <ion-list-header>\n      <h5 style=\"font-size: 17px;font-weight: bold;\" *ngIf=\"total > 0\">Total: ${{PrecioTotal}}</h5>\n    </ion-list-header>\n\n    <ion-grid *ngFor=\"let item of verPedido; let i = index\">\n      <ion-item style=\"margin-top: 3%;\" *ngIf=\"item.cantidad > 0\">\n        <ion-thumbnail slot=\"start\">\n          <img [src]=\"item.imagen\">\n        </ion-thumbnail>\n        <ion-label>\n          <h2>ID Producto: {{item.id}}</h2>\n          <h2>Precio del producto ${{item.precio | number}}</h2>\n          <h2>Cantidad: {{item.cantidad}}</h2>\n          <h2>{{item.titulo}}</h2>\n          <h2 style=\"margin-top: 2%;background: darkgoldenrod;border-radius: 3px;padding: 2px;text-align: center;\">Total: ${{item.precio*item.cantidad | number}}</h2>\n        </ion-label>\n      </ion-item>\n    </ion-grid>\n    <ion-col *ngIf=\"total == 0\" style=\"margin: auto;display: block;\">\n      <ion-icon style=\"font-size:120px;color: rgb(173, 170, 170);margin: auto;display: block;margin-top: 5%;\" name=\"cart\"></ion-icon>\n      <p style=\"text-align: center;padding: 5%;\">Lo sentimos, no tienes empanadas agregadas al carrito</p>\n      <ion-button (click)=\"agregarAlCarrrito()\" expand=\"block\" color=\"success\">Agregar  <ion-icon name=\"add\"></ion-icon></ion-button>\n    </ion-col>\n    <ion-row *ngIf=\"!total == 0\"> \n      <ion-col>\n        <div>\n          <ion-button (click)=\"vaciarProducto()\" expand=\"block\" color=\"primary\">VACIAR CARRITO</ion-button>\n        </div>\n      </ion-col>\n      <ion-col>\n        <div>\n          <ion-button (click)=\"enviarProductos()\" expand=\"block\" color=\"success\">ENVIAR PEDIDO</ion-button>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n\n  <ion-list id=\"formulario\" style=\"display: none;\">\n\n    <ion-grid>\n  \n      <ion-item style=\"margin-top: 4%;\">\n        <ion-label position=\"stacked\">Nombres <ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-input [(ngModel)]=\"nombres\" type=\"text\"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label position=\"stacked\">Dirección <ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-input [(ngModel)]=\"direccion\" type=\"text\"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label position=\"stacked\">Teléfono <ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-input [(ngModel)]=\"telefono\" type=\"number\"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label position=\"stacked\">Correo electronico <ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-input [(ngModel)]=\"email\" type=\"email\"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label position=\"stacked\">Nota <ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-textarea [(ngModel)]=\"nota\" type=\"text\"></ion-textarea>\n      </ion-item>\n\n      <ion-row style=\"margin-top: 10%;\">\n        <ion-col>\n          <ion-button (click)=\"VolverPedido()\" expand=\"block\" color=\"warning\"><ion-icon name=\"arrow-back\"></ion-icon>Volver</ion-button>        \n        </ion-col>\n        <ion-col>\n          <ion-button (click)=\"EnviarPedido()\" expand=\"block\" color=\"success\">Enviar <ion-icon style=\"margin-left: 3%;\" name=\"paper-plane\"></ion-icon></ion-button>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-list>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/pedido/pedido.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/pages/pedido/pedido.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesPedidoPedidoPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BlZGlkby9wZWRpZG8ucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/pedido/pedido.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/pages/pedido/pedido.page.ts ***!
    \*********************************************/

  /*! exports provided: PedidoPage */

  /***/
  function srcAppPagesPedidoPedidoPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PedidoPage", function () {
      return PedidoPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/service/api-services.service */
    "./src/app/service/api-services.service.ts");

    var PedidoPage = /*#__PURE__*/function () {
      function PedidoPage(router, modalCtrl, navParams, alertController, toastController, servicios, loadingController) {
        _classCallCheck(this, PedidoPage);

        this.router = router;
        this.modalCtrl = modalCtrl;
        this.alertController = alertController;
        this.toastController = toastController;
        this.servicios = servicios;
        this.loadingController = loadingController;
        this.verPedido = [];
        this.PrecioTotal = 0;
        this.TotalPrecio = 0;
        this.Home = [];
        this.itemTotal = 0;
        this.sumatoria(navParams);
      }

      _createClass(PedidoPage, [{
        key: "sumatoria",
        value: function sumatoria(navParams) {
          this.total = navParams.get('total');
          this.verPedido = navParams.get('carrito');

          var _iterator = _createForOfIteratorHelper(this.verPedido),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var pedido = _step.value;
              // this.PrecioTotal += pedido.precio*pedido.cantidad;
              this.TotalPrecio += pedido.precio * pedido.cantidad;
              this.PrecioTotal = this.TotalPrecio.toLocaleString('de-DE');
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          if (this.total == 0) {
            this.verPedido = [];
            this.PrecioTotal = 0;
          }
        }
      }, {
        key: "limpiarProuctos",
        value: function limpiarProuctos() {
          this.verPedido = [];
          this.total = 0;
        }
      }, {
        key: "VolverPedido",
        value: function VolverPedido() {
          document.getElementById("listado").style.display = "block";
          document.getElementById("formulario").style.display = "none";
        }
      }, {
        key: "enviarProductos",
        value: function enviarProductos() {
          document.getElementById("listado").style.display = "none";
          document.getElementById("formulario").style.display = "block";
        }
      }, {
        key: "dismiss",
        value: function dismiss() {
          this.modalCtrl.dismiss({
            'carrito': true
          });
        }
      }, {
        key: "cerrar",
        value: function cerrar() {
          if (this.verPedido.length == 0) {
            this.modalCtrl.dismiss({
              'carrito': true
            });
          } else if (this.verPedido.length > 0) {
            this.modalCtrl.dismiss({
              'carrito': false
            });
          }
        }
      }, {
        key: "agregarAlCarrrito",
        value: function agregarAlCarrrito() {
          this.dismiss();
        }
      }, {
        key: "vaciarProducto",
        value: function vaciarProducto() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.alertController.create({
                      cssClass: 'my-custom-class',
                      header: 'Espera',
                      message: '<strong>Esta seguro que desea eliminar tus productos</strong>',
                      buttons: [{
                        text: 'NO',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: function handler(blah) {}
                      }, {
                        text: 'SI',
                        handler: function handler() {
                          _this.limpiarProuctos();
                        }
                      }]
                    });

                  case 2:
                    alert = _context.sent;
                    _context.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "showToastRegistroMensaje",
        value: function showToastRegistroMensaje(text) {
          this.toast = this.toastController.create({
            message: text,
            duration: 2000,
            position: 'top'
          }).then(function (toastData) {
            toastData.present();
          });
        }
      }, {
        key: "EnviarPedido",
        value: function EnviarPedido() {
          var _this2 = this;

          if (!this.nombres) {
            this.showToastRegistroMensaje('Porfavor llenar el campo nombres');
          } else if (!this.direccion) {
            this.showToastRegistroMensaje('Porfavor llenar el campo dirección');
          } else if (!this.telefono) {
            this.showToastRegistroMensaje('Porfavor llenar el campo telefono');
          } else if (!this.email) {
            this.showToastRegistroMensaje('Porfavor llenar el campo email');
          } else if (!this.nota) {
            this.showToastRegistroMensaje('Porfavor llenar el campo nota');
          } else {
            var params = {
              nombres: this.nombres,
              direccion: this.direccion,
              telefono: this.telefono,
              email: this.email,
              nota: this.nota,
              precioTotalRecetas: this.PrecioTotal,
              recetas: this.verPedido
            };
            this.Presentloading();
            this.servicios.enviarPedido(params).subscribe(function (data) {
              if (data['logger'] == true) {
                _this2.showToast(data['msg']);

                _this2.limpiarProuctos();

                _this2.salirLoading();
              } else {
                _this2.showToast(data['msg']);
              }

              _this2.nombres = "";
              _this2.direccion = "";
              _this2.telefono = "";
              _this2.email = "";
              _this2.nota = "";
            });
          }
        }
      }, {
        key: "Presentloading",
        value: function Presentloading() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.create({
                      message: 'Cargando...'
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "salirLoading",
        value: function salirLoading() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.loadingController.dismiss();

                  case 2:
                    return _context3.abrupt("return", _context3.sent);

                  case 3:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "showToast",
        value: function showToast(text) {
          this.toast = this.toastController.create({
            message: text,
            duration: 2000,
            position: 'top'
          }).then(function (toastData) {
            toastData.present();
          });
        }
      }, {
        key: "HideToast",
        value: function HideToast() {
          this.toast = this.toastController.dismiss();
        }
      }]);

      return PedidoPage;
    }();

    PedidoPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_4__["ApiServicesService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }];
    };

    PedidoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-pedido',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./pedido.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pedido/pedido.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./pedido.page.scss */
      "./src/app/pages/pedido/pedido.page.scss"))["default"]]
    })], PedidoPage);
    /***/
  },

  /***/
  "./src/app/service/api-services.service.ts":
  /*!*************************************************!*\
    !*** ./src/app/service/api-services.service.ts ***!
    \*************************************************/

  /*! exports provided: ApiServicesService */

  /***/
  function srcAppServiceApiServicesServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApiServicesService", function () {
      return ApiServicesService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var ApiServicesService = /*#__PURE__*/function () {
      function ApiServicesService(http) {
        _classCallCheck(this, ApiServicesService);

        this.http = http;
        this.cabecera = new Headers();
        this.cabecera.append('Access-Control-Allow-Origin', '*');
        this.cabecera.append("Cache-Control", "no-cache");
        this.cabecera.append("Access-Control-Allow-Methods", 'GET,POST,OPTIONS,DELETE,PUT');
        this.cabecera.append("Access-Control-Allow-Headers", "gid, filename, Origin, X - Requested - With, Content - Type, Accept, Authorization");
      }

      _createClass(ApiServicesService, [{
        key: "getHome",
        value: function getHome() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=28&tags=27', {
            headers: this.cabecera
          });
        }
      }, {
        key: "enviarComentario",
        value: function enviarComentario(params) {
          return this.http.post('https://mpglobal.info/servicio.php', params, {
            headers: this.cabecera
          });
        }
      }, {
        key: "getImagenes",
        value: function getImagenes() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=30&tags=29', {
            headers: this.cabecera
          });
        }
      }, {
        key: "getContacto",
        value: function getContacto() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=31&tags=32', {
            headers: this.cabecera
          });
        }
      }, {
        key: "getModal",
        value: function getModal() {
          return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=33&tags=34', {
            headers: this.cabecera
          });
        } // Envio de Pedido

      }, {
        key: "enviarPedido",
        value: function enviarPedido(params) {
          return this.http.post('https://empanadas.bar/servicios_empanadas_bar/servicios_envio_pedido.php', params, {
            headers: this.cabecera
          });
        }
      }]);

      return ApiServicesService;
    }();

    ApiServicesService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    ApiServicesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], ApiServicesService);
    /***/
  }
}]);
//# sourceMappingURL=default~pages-inicio-inicio-module~pages-pedido-pedido-module-es5.js.map