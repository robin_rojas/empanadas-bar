(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contacto-contacto-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contacto/contacto.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contacto/contacto.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>contacto</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n\n  <ion-card style=\"padding: 0px;margin-top: 12%;\" *ngFor=\"let item of contact;let i = index\">\n    <img [src]=\"item.featured_image.size_large\" width=\"100%\">     \n    <h3 style=\"text-align: center;\" [innerHTML]=\"item.title.rendered\"></h3>  \n    <div style=\"text-align:justify;font-size:15px;padding:5px;\" [innerHTML]=\"item.excerpt.rendered\"></div> \n  </ion-card>\n\n<ion-col style=\"display: flex;justify-content: center;\"> \n  <a href=\"https://wa.me/+1(832)973-2385\"><ion-button color=\"success\" expand=\"block\"><ion-icon style=\"color: #fff\" name=\"logo-whatsapp\"></ion-icon></ion-button></a>\n  <ion-button (click)=\"abrirFacebook()\" color=\"primary\" expand=\"block\"><ion-icon style=\"color: #fff\" name=\"logo-facebook\"></ion-icon></ion-button>\n</ion-col>\n\n<ion-grid style=\"margin-top: 2%;\">\n  <ion-row>\n    <ion-col style=\"text-align: center;\">\n      <ion-icon style=\"font-size: 35px;\" name=\"mail\"></ion-icon><p>Info@empanadas.bar</p>\n    </ion-col>\n    <ion-col style=\"text-align: center;\">\n      <ion-icon style=\"font-size: 35px;\" name=\"call\"></ion-icon><p>844-674-5622</p>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n</ion-content>\n\n<app-tab></app-tab>\n");

/***/ }),

/***/ "./src/app/pages/contacto/contacto-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/contacto/contacto-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: ContactoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageRoutingModule", function() { return ContactoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/contacto/contacto.page.ts");




const routes = [
    {
        path: '',
        component: _contacto_page__WEBPACK_IMPORTED_MODULE_3__["ContactoPage"]
    }
];
let ContactoPageRoutingModule = class ContactoPageRoutingModule {
};
ContactoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/contacto/contacto.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/contacto/contacto.module.ts ***!
  \***************************************************/
/*! exports provided: ContactoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageModule", function() { return ContactoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacto-routing.module */ "./src/app/pages/contacto/contacto-routing.module.ts");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/contacto/contacto.page.ts");
/* harmony import */ var src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/tab/tab.component */ "./src/app/components/tab/tab.component.ts");








let ContactoPageModule = class ContactoPageModule {
};
ContactoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactoPageRoutingModule"]
        ],
        declarations: [_contacto_page__WEBPACK_IMPORTED_MODULE_6__["ContactoPage"], src_app_components_tab_tab_component__WEBPACK_IMPORTED_MODULE_7__["TabComponent"]]
    })
], ContactoPageModule);



/***/ }),

/***/ "./src/app/pages/contacto/contacto.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/contacto/contacto.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbnRhY3RvL2NvbnRhY3RvLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/contacto/contacto.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/contacto/contacto.page.ts ***!
  \*************************************************/
/*! exports provided: ContactoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPage", function() { return ContactoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/api-services.service */ "./src/app/service/api-services.service.ts");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");





let ContactoPage = class ContactoPage {
    constructor(servicios, iab, loadingController) {
        this.servicios = servicios;
        this.iab = iab;
        this.loadingController = loadingController;
        this.contact = [];
    }
    ngOnInit() {
        this.listadoContact();
    }
    listadoContact() {
        this.Presentloading();
        this.servicios.getContacto()
            .subscribe((data) => {
            this.contact = data;
            console.log(this.contact);
            this.salirLoading();
        });
    }
    abrirFacebook() {
        const options = {
            closebuttoncaption: 'ATRÁS/BACK',
            fullscreen: "yes",
            closebuttoncolor: '#F2F7F7',
            footer: 'no',
            mediaPlaybackRequiresUserAction: 'yes',
            location: 'no',
            hidden: 'no',
            hideurlbar: 'yes',
        };
        this.iab.create('https://www.facebook.com/ENTRENAN/', '_system', options);
    }
    Presentloading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
                duration: 1000,
            });
            yield loading.present();
        });
    }
    salirLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield this.loadingController.dismiss();
        });
    }
};
ContactoPage.ctorParameters = () => [
    { type: src_app_service_api_services_service__WEBPACK_IMPORTED_MODULE_3__["ApiServicesService"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__["InAppBrowser"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
ContactoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contacto',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contacto.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contacto/contacto.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contacto.page.scss */ "./src/app/pages/contacto/contacto.page.scss")).default]
    })
], ContactoPage);



/***/ }),

/***/ "./src/app/service/api-services.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/api-services.service.ts ***!
  \*************************************************/
/*! exports provided: ApiServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiServicesService", function() { return ApiServicesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



let ApiServicesService = class ApiServicesService {
    constructor(http) {
        this.http = http;
        this.cabecera = new Headers();
        this.cabecera.append('Access-Control-Allow-Origin', '*');
        this.cabecera.append("Cache-Control", "no-cache");
        this.cabecera.append("Access-Control-Allow-Methods", 'GET,POST,OPTIONS,DELETE,PUT');
        this.cabecera.append("Access-Control-Allow-Headers", "gid, filename, Origin, X - Requested - With, Content - Type, Accept, Authorization");
    }
    getHome() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=28&tags=27', { headers: this.cabecera });
    }
    enviarComentario(params) {
        return this.http.post('https://mpglobal.info/servicio.php', params, { headers: this.cabecera });
    }
    getImagenes() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=30&tags=29', { headers: this.cabecera });
    }
    getContacto() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=31&tags=32', { headers: this.cabecera });
    }
    getModal() {
        return this.http.get('https://drpimentel.com/index.php/wp-json/wp/v2/posts?per_page=100&categories=33&tags=34', { headers: this.cabecera });
    }
    // Envio de Pedido
    enviarPedido(params) {
        return this.http.post('https://empanadas.bar/servicios_empanadas_bar/servicios_envio_pedido.php', params, { headers: this.cabecera });
    }
};
ApiServicesService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiServicesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiServicesService);



/***/ })

}]);
//# sourceMappingURL=pages-contacto-contacto-module-es2015.js.map