(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-ver-home-ver-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/ver-home/ver-home.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/ver-home/ver-home.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" routerLink=\"/inicio\"></ion-back-button>\n    </ion-buttons>\n    <ion-title style=\"font-size: 13px;\"></ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-col>\n    <img [src]=\"verHome.featured_image.size_large\">    \n    <h3 style=\"text-align: center;\" [innerHTML]=\"verHome.title.rendered\"></h3> \n    <p class=\"textLista\" style=\"text-align:justify;font-size:15px;padding:5%\" [innerHTML]=\"verHome.content.rendered\"></p>  \n  </ion-col>\n\n  <ion-col style=\"display: flex;align-items: center;\">\n    <ion-label style=\"text-align: center;\">Envia tu comentario sobre EMPANADAS BAR, recibiremos tus sugerencias de la mejor manera</ion-label>\n  </ion-col>\n\n  <ion-col>\n    <ion-Button routerLink=\"/comentario\" style=\"margin-top: 5%;\" expand=\"block\" fill=\"outline\" color=\"success\">Comentario</ion-Button> \n  </ion-col>\n \n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/ver-home/ver-home-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/ver-home/ver-home-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: VerHomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerHomePageRoutingModule", function() { return VerHomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ver_home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ver-home.page */ "./src/app/pages/ver-home/ver-home.page.ts");




const routes = [
    {
        path: '',
        component: _ver_home_page__WEBPACK_IMPORTED_MODULE_3__["VerHomePage"]
    }
];
let VerHomePageRoutingModule = class VerHomePageRoutingModule {
};
VerHomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], VerHomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/ver-home/ver-home.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/ver-home/ver-home.module.ts ***!
  \***************************************************/
/*! exports provided: VerHomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerHomePageModule", function() { return VerHomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ver_home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ver-home-routing.module */ "./src/app/pages/ver-home/ver-home-routing.module.ts");
/* harmony import */ var _ver_home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ver-home.page */ "./src/app/pages/ver-home/ver-home.page.ts");







let VerHomePageModule = class VerHomePageModule {
};
VerHomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ver_home_routing_module__WEBPACK_IMPORTED_MODULE_5__["VerHomePageRoutingModule"]
        ],
        declarations: [_ver_home_page__WEBPACK_IMPORTED_MODULE_6__["VerHomePage"]]
    })
], VerHomePageModule);



/***/ }),

/***/ "./src/app/pages/ver-home/ver-home.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/ver-home/ver-home.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Zlci1ob21lL3Zlci1ob21lLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/ver-home/ver-home.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/ver-home/ver-home.page.ts ***!
  \*************************************************/
/*! exports provided: VerHomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerHomePage", function() { return VerHomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let VerHomePage = class VerHomePage {
    constructor(router) {
        this.router = router;
        if (this.router.getCurrentNavigation().extras.state) {
            this.verHome = this.router.getCurrentNavigation().extras.state.itemHome;
            console.log(this.verHome);
        }
    }
    ngOnInit() {
    }
};
VerHomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
VerHomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ver-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./ver-home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/ver-home/ver-home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./ver-home.page.scss */ "./src/app/pages/ver-home/ver-home.page.scss")).default]
    })
], VerHomePage);



/***/ })

}]);
//# sourceMappingURL=pages-ver-home-ver-home-module-es2015.js.map